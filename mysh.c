/*
 * Elise Baumgartner
 * Sahee Thao
 * CS441/541: Project 3
 *
 */
#include "mysh.h"

/**
 * Parse command line arguments passed to myshell upon startup.
 *
 * @param argc : Number of command line arguments, argv : Array of pointers to strings
 * @return 0 on success, negative value on error
 */
int main (int argc, char * argv[]) {
    is_debug = FALSE;

    /*
     * Parse Command line arguments to check if this is an interactive or batch
     * mode run.
     */
    if (0 != parse_args_main(argc, argv)) {
        fprintf(stderr, "Error: Invalid command line!\n");
        return -1;
    }

    /*
     * If in batch mode then process all batch files
     */
    if (TRUE == is_batch) {
        if ( TRUE == is_debug ) {
            printf("Batch Mode!\n");
        }
        int i;
        for(i = 1; i < argc; i++) {
          fp_in = NULL;
          fp_in = fopen(argv[i], "r");
          if (0 != batch_mode()) {
              fprintf(stderr, "Error: Batch mode returned a failure!\n");
          }
        }

    }
    /*
     * Otherwise proceed in interactive mode
     */
    else if ( FALSE == is_batch ) {
        if ( TRUE == is_debug ) {
            printf("Interactive Mode!\n");
        }

        if (0 != interactive_mode()) {
            fprintf(stderr, "Error: Interactive mode returned a failure!\n");
        }
    }
    /*
     * This should never happen, but otherwise unknown mode
     */
    else {
        fprintf(stderr, "Error: Unknown execution mode!\n");
        return -1;
    }

    free_history();
    /*
     * Display counts
     */
    printf("-------------------------------\n");
    printf("Total number of jobs               = %d\n", total_jobs);
    printf("Total number of jobs in history    = %d\n", total_history);
    printf("Total number of jobs in background = %d\n", total_jobs_bg);

    /*
     * Cleanup
     */
     /*
    if( NULL != batch_files) {
        free(batch_files);
        batch_files = NULL;
        num_batch_files = 0;
    }
    */

    return 0;
}

int parse_args_main(int argc, char **argv)
{

    /*
     * If no command line arguments were passed then this is an interactive
     * mode run.
     */

     if (argc == 1) {
       is_batch = FALSE;
       return 0;

       /*
        * If command line arguments were supplied then this is batch mode.
        */
     } else {
       char *file = argv[1];
       int end = strlen(file) - 1;
       if (file[end] == 't' && file[end - 1] == 'x' && file[end - 2] == 't' && file[end - 3] == '.') {
         fp_in = fopen(file, "r");
         is_batch = TRUE;
         return 0;
       } else {
         return -1;
       }
     }
}
/*
 * Main method for batch mode
 *
 * @return 0 on success, negative value on error
 */
int batch_mode(void)
{
    /*
     * For each file...
     */

     size_t bufsize = 2048;
     char *buffer = malloc(bufsize * sizeof(char));
     int is_exit = FALSE;
     if(fp_in != NULL) {
       job_t *job_list = NULL;
       while(fgets(buffer, 2048, fp_in)) {
         /* Check length */
         if(strlen(buffer) > 1024) {
           printf("Command too long [max = 1024]\n");
           continue;
         }

         /* Strip off the newline */
         int i;
         for(i = 0; i < bufsize; i++) {
           if(buffer[i] == 13) {
             buffer[i] = '\0';
             break;
           }
         }


         if(i == 0) {
           continue;
         }

         /*
          * Parse for multiple jobs in one line
          */

          for(i = 0; i < strlen(buffer); i++) {
            if(buffer[i] == ';') {
              if(i == 0) {
                memmove(buffer, buffer+1, strlen(buffer));
              } else if(i == (strlen(buffer) - 1)) {
                buffer[strlen(buffer) - 1] = '\0';
              } else{
                /*Semicolon not in the beginning or end*/
                if(buffer[i + 1] != ' ') {
                  /*No space after the ;*/
                  int j;
                  for(j = strlen(buffer); j > i; j--) {
                    buffer[j + 1] = buffer[j];
                  }
                  buffer[i + 1] = ' ';
                } if(buffer[i - 1] != ' ') {
                  /*No space before the ;*/
                  int j;
                  for(j = strlen(buffer); j > i; j--) {
                    buffer[j + 1] = buffer[j];
                  }
                  buffer[i + 1] = ';';
                  buffer[i] = ' ';
                }
              }
            }
          }

          for(i = 0; i < strlen(buffer); i += 1) {
            if(buffer[i] == '&') {
              insert_space(buffer, i);
              i += 2;
            }
          }
          job_list = break_up_line(buffer); /* Takes buffer and breaks up jobs */
          job_t *cur_job = job_list;
          while(cur_job != NULL) {
           total_history += 1;
         /*adding job to history*/
           history_node *node = malloc(sizeof(history_node));
           node->next_node = NULL;
           job_t *new_job = create_job();
           copy_job_contents(new_job, cur_job);
           node->job = new_job;
           if (history_list == NULL) {
             history_list = node;
             history_list_end = node;
           } else {
             history_list_end->next_node = node;
             history_list_end = node;
           }
           /*executing command*/
             if(strcmp(cur_job->binary, "exit") == 0) {
               is_exit = TRUE;
               break;
             } else if (strcmp(cur_job->binary, "jobs") == 0){
               builtin_jobs();
             } else if (strcmp(cur_job->binary, "history") == 0){
               builtin_history();
             } else if (strcmp(cur_job->binary, "wait") == 0) {
               builtin_wait(0);
             } else if (strcmp(cur_job->binary, "fg") == 0) {
               builtin_fg(cur_job);
             } else {
               if(execute_command(cur_job) != 0) {
                 printf("Error in Executing Command\n");
               }
             }
           cur_job = cur_job->next_job;
         }
         if(is_exit == TRUE) {
           builtin_exit();
           break;
         }
         check_bgp();
       }
       builtin_wait(0);
     } else {
       printf("Error opening file\n");
     }

        /*
         * Open the batch file
         * If there was an error then print a message and move on to the next file.
         * Otherwise,
         *   - Read one line at a time.
         *   - strip off new line
         *   - parse and execute
         * Close the file
         */

    /*
     * Cleanup
     */


    return 0;
}

/**
 * Main method for interactive mode
 * @return 0 on success, negative value on error
 */
int interactive_mode(void)
{

    size_t bufsize = 64;
    size_t characters;
    char *buffer = malloc(bufsize * sizeof(char));
    int is_exit = FALSE;

    job_t *job_list = NULL;

    do {
        /*
         * Print the prompt
         */

        /* check_bgp(); */

        if (buffer == NULL) {
          perror("Unable to allocate buffer");
          exit(1);
        }
        printf("%s", PROMPT);

        /*
         * Read stdin, break out of loop if Ctrl-D
         */

        characters = getline(&buffer,&bufsize,stdin);

        /* Check length */
        if(strlen(buffer) > 1024) {
          printf("Command too long [max = 1024]\n");
          continue;
        }

        /* Strip off the newline */
        int i;
        for(i = 0; i < bufsize; i++) {
          if(buffer[i] == '\n') {
            buffer[i] = '\0';
            break;
          }
        }
        if (characters == EOF) {
          printf("logout\n");
          builtin_exit();
          break;
        }

        if(i == 0) {
          continue;
        }

        /*
         * Parse for multiple jobs in one line
         */

         for(i = 0; i < strlen(buffer); i++) {
           if(buffer[i] == ';') {
             if(i == 0) {
               memmove(buffer, buffer+1, strlen(buffer));
             } else if(i == (strlen(buffer) - 1)) {
               buffer[strlen(buffer) - 1] = '\0';
             } else{
               /*Semicolon not in the beginning or end*/
               if(buffer[i + 1] != ' ') {
                 /*No space after the ;*/
                 int j;
                 for(j = strlen(buffer); j > i; j--) {
                   buffer[j + 1] = buffer[j];
                 }
                 buffer[i + 1] = ' ';
               } if(buffer[i - 1] != ' ') {
                 /*No space before the ;*/
                 int j;
                 for(j = strlen(buffer); j > i; j--) {
                   buffer[j + 1] = buffer[j];
                 }
                 buffer[i + 1] = ';';
                 buffer[i] = ' ';
               }
             }
           }
         }

         for(i = 0; i < strlen(buffer); i += 1) {
           if(buffer[i] == '&') {
             insert_space(buffer, i);
             i += 2;
           }
         }


         job_list = break_up_line(buffer); /* Takes buffer and breaks up jobs */

         job_t *cur_job = job_list;
         //job_t* history_job = malloc(sizeof(job_t *));

         while(cur_job != NULL) {
           total_history += 1;
         /*adding job to history*/
           history_node *node = malloc(sizeof(history_node));
           node->next_node = NULL;
           job_t *new_job = create_job();
           copy_job_contents(new_job, cur_job);
           node->job = new_job;
           if (history_list == NULL) {
             history_list = node;
             history_list_end = node;
           } else {
             history_list_end->next_node = node;
             history_list_end = node;
           }
           /*executing command*/
             if(strcmp(cur_job->binary, "exit") == 0) {
               is_exit = TRUE;
               break;
             } else if (strcmp(cur_job->binary, "jobs") == 0){
               builtin_jobs();
             } else if (strcmp(cur_job->binary, "history") == 0){
               builtin_history();
             } else if (strcmp(cur_job->binary, "wait") == 0) {
               builtin_wait(0);
             } else if (strcmp(cur_job->binary, "fg") == 0) {
               builtin_fg(cur_job);
             } else {
               if(execute_command(cur_job) != 0) {
                 printf("Error in Executing Command\n");
               }
             }
           cur_job = cur_job->next_job;
         }
         if(is_exit == TRUE) {
           builtin_exit();
           break;
         }
         check_bgp();
    } while(characters != EOF/* end condition */);

    /*
     * Cleanup
     */

    free(buffer);
    return 0;
}

/**
 * This method inserts spaces surround a character in a string at an index.
 * Used for '&' -> ' & '
 * @param str is the string we will be changing,
 * index is the index of the string we will add spaces surrounding it
 */
void insert_space(char *str, int index) {
  int len = strlen(str);
  char second[64];
  char index_c = str[index];
  second[0] = ' ';
  second[1] = index_c;
  second[2] = ' ';
  int i;
  for (i = 3; i < len; i += 1) {
    second[i] = str[index + i - 3 + 1];
  }
  second[len] = '\0';
  /* printf("second[] = \"%s\"\n", second); */
  int j = 0;
  for (i = index; j < strlen(second); i += 1) {
    str[i] = second[j];
    j += 1;
  }
  str[i] = '\0';
}
/*
 * @param copies the value of job to empty job
 */
void copy_job_contents(job_t *empty_job, job_t *job) {
  int i;
  for(i = 0; i < job->argc; i += 1) {
    empty_job->argv[i] = strdup(job->argv[i]);
  }
  empty_job -> argc = job -> argc;
  empty_job -> is_background = job -> is_background;
}

/**
 * Executes a job by forking.
 * @param job is used for executing
 */
int execute_command(job_t * job) {
  total_jobs += 1;
  pid_t c_pid = 0;
  int status = 0;
  c_pid = fork();
  if (c_pid < 0) {
    fprintf(stderr, "fork failed\n");
    return -1;
  } else if (c_pid == 0) {
    execvp(job->binary, job->argv);
    if (!job->is_background) {
      int i;
      for (i = 0; i < job->argc; i += 1) {
        printf("%s ", job->argv[i]);
      }
      printf(": command not found\n");
    }
    exit(1);
  } else {
    if (!job->is_background) {
      waitpid(c_pid, &status, 0);
    } else {
      total_jobs_bg += 1;
      add(c_pid, job);
      bgp *cur_p = bgp_list;
      int index = 1;
      while (cur_p != NULL) {
        if (c_pid == cur_p->c_pid) {
          printf("[%d] %d %s\n", index, c_pid, job->binary);
        }
        index += 1;
        cur_p = cur_p->next_bgp;
      }
      check_bgp();
    }
  }
  return 0;
}

/**
 * Used for debugging.
 * @param str is optional printing.
 */
void print_bgp_list(char *str) {
  if (is_debug) {
    printf("%s\n", str);
    bgp *cur_bgp = bgp_list;
    while (cur_bgp != NULL) {
      printf("[%d] -> ", cur_bgp->c_pid);
      cur_bgp = cur_bgp->next_bgp;
    }
    if (cur_bgp == NULL) {
      printf("[NULL]");
      printf("\n");
    }
  }
}

/**
 * Adds a bgp node to the bgp_list.
 * This keeps track of background processes in a linked list.
 * @param c_pid used for applying data to bpg node, job is used to add data to bgp node
 */
void add(int c_pid, job_t *job) {
  bgp *bg_process = malloc(sizeof(bgp));
  if (bgp_list == NULL) {
    bgp_list = bg_process;
  } else {
    bgp_list_end->next_bgp = bg_process;
  }
  bg_process->c_pid = c_pid;
  bg_process->is_running = TRUE;
  bg_process->failed = FALSE;

  /* This section converts a char ** to a char *. This is the command. */
  int i;
  int len = 0;
  char *command = malloc(sizeof(char) * 32);
  command[0] = '\0';

  for (i = 0; i < job->argc; i += 1) {
    char *arg = strdup(job->argv[i]);

    int arglen = strlen(arg);
    char *part = malloc(sizeof(char) * (arglen + 2));
    strcpy(part, arg);

    free(arg);
    part[arglen] = ' ';
    part[arglen + 1] = '\0';
    len += strlen(part);

    char *temp = malloc(sizeof(char) * (len + 1));
    int j;
    for (j = 0; j < strlen(command); j += 1) {
      temp[j] = command[j];
    }
    for (j = strlen(command); j < len; j += 1) {
      temp[j] = part[j - strlen(command)];
    }
    temp[len] = '\0';
    free(command);
    free(part);
    command = temp;
  }

  bg_process->command = strdup(command);
  free(command);


  bg_process->next_bgp = NULL;
  bgp_list_end = bg_process;

  print_bgp_list("Adding finished.");
}

/**
 * This method sets the status of is_running, failed of background processes
 * in the list bgp_list.
 */
void check_bgp() {
  bgp *cur_p = bgp_list;

  if (cur_p == NULL) {
    return;
  }

  while (cur_p != NULL) {
    if (cur_p->is_running == 1) {
      /* printf("Looking at process [%d, %s]\n", cur_p->c_pid, cur_p->command); */
      int status;
      if (waitpid(cur_p->c_pid, &status,WNOHANG) != 0) {
        int exited = WIFEXITED(status);
        int is_running = 0;
        int failed = 0;
        if (exited) {
          is_running = 0;
          failed = WEXITSTATUS(status);
          if (failed == 1) {
            failed = 1;
          } else {
            failed = 0;
          }
        } else {
          is_running = 1;
        }
         if (failed == 1) {
          cur_p->is_running = FALSE;
          if (cur_p->failed == TRUE) {
            /* printf("%s: command not found\n", cur_p->command); */
          }
          cur_p->failed = TRUE;
        } else if (is_running == 1) {
          cur_p->is_running = TRUE;
          cur_p->failed = -1;
        } else if (is_running == 0){
          cur_p->is_running = FALSE;
          cur_p->failed = FALSE;
        }
      } else {
        cur_p->is_running = TRUE;
        cur_p->failed = -1;
      }

    }
    /* printf("Status of [%d, %s]: %d | %d\n", cur_p->c_pid, cur_p->command, cur_p->is_running, cur_p->failed); */
    cur_p = cur_p->next_bgp;
    if (cur_p == NULL) {
      break;
    }
  }
}

/**
 * Mallocs and creates a job struct.
 * @return job_t is a blank job_t
 */
job_t* create_job() {
  size_t bufsize = 2048;

  job_t *job = NULL;
  job = malloc(sizeof(job_t));
  job->full_command = NULL;
  job->argc = 0;
  job->argv = malloc(sizeof(char*) * bufsize);
  job->binary = NULL;
  job->is_background = FALSE;
  job->next_job = NULL;
  return job;
}
/**
 * @param job to be freed.
 */
void free_job(job_t *job) {
  job_t *cur = job;
  job_t *prev;
  while (cur != NULL) {
    prev = cur;
    cur = cur->next_job;
    free(prev->argv);
    free(prev);
  }
}

/**
 * Frees history list
 */
void free_history() {
  history_node *cur = history_list;
  history_node *prev;
  while (cur != NULL) {
    prev = cur;
    cur = cur->next_node;
    free_job(prev->job);
    free(prev);
  }
  history_list = cur;
  history_list_end = cur;
}
/**
 * Takes a full command and breaks up to each command and it's arguements
 * splitting into individual jobs when needed.
 * @return the head of a job linked list
 */
job_t* break_up_line(char * fc)
{
  /* Create and malloc job */
  job_t *job_list_head = create_job();

  const char s[2] = " ";
  char *token;

  job_list_head->full_command = fc;

  job_t *cur_job = job_list_head;
  job_t *prev_job = NULL;
  /* printf("fc: %s\n", fc); */
  token = strtok(fc, s);
  int count = 0;
  int bg_next_look = FALSE; /* Flag used for finding out if it is the case where another command is after a bg process. Example sleep 1 & vs sleep 1 & sleep 2 & */
  while (token != NULL) {
    if (bg_next_look) {
      bg_next_look = FALSE;
    }
    if(token[0] == ';'  || token[0] == '&') {
      /*set old job values*/
      cur_job->argc = count;
      if (token[0] == '&') {
          cur_job->is_background = TRUE;
          cur_job->argv[count] = NULL;
          bg_next_look = TRUE;
      } else {
        cur_job->is_background = FALSE;
        cur_job->argv[count] = NULL;
      }

      count = 0;

      /*link the list*/
      prev_job = cur_job;
      job_t *new_job = create_job();
      cur_job->next_job = new_job;
      cur_job = new_job;

    } else {
      if(count == 0) {
        cur_job->argv[0] = token;
        cur_job->binary = token;
        count += 1;
      } else {
        cur_job->argv[count] = token;
        count += 1;
      }
    }
    token = strtok(NULL, s);
  }
  cur_job->argv[count] = NULL;
  cur_job->argc = count;

  if (bg_next_look) {
    free_job(cur_job);
    prev_job->next_job = NULL;
  }
  return job_list_head;
}

/**
 * Displays the number a background processes that are being waited for.
 * Then the background processes are cleaned up.
 */
int builtin_exit(void)
{
  builtin_wait(1);
  builtin_jobs();
  return 0;
}

/**
 * This method prints out the order in the background processes were called
 * and displays their current status. Running (valid running command),
 * done (valid finished command) or exit (invalid command).
 * Then the done and exit commands are cleaned up and removed from the background
 * process list.
 * Frees bgp_list.
 */
int builtin_jobs(void) {
  check_bgp(); /* Updates the bgp_list */

  bgp *cur_p = bgp_list;
  if (cur_p == NULL) {
    return 0;
  }

  /* There exists a list */
  int index = 1;
  while (cur_p != NULL) {
    /* set index */
    cur_p->index = index;
    index += 1;

    /* set order */
    if (cur_p->next_bgp == NULL) {
      cur_p->order = 2;
    } else if (cur_p->next_bgp->next_bgp == NULL) {
      cur_p->order = 1;
    } else {
      cur_p->order = 0;
    }
    cur_p = cur_p->next_bgp;
  }

  /* Set cur to head */
  cur_p = bgp_list;

  while (cur_p != NULL) {
    char c = ' ';
    if (cur_p->order == 2) {
      c = '+';
    } else if (cur_p->order == 1) {
      c = '-';
    }
    char *state;
    char amp = ' ';
    if (cur_p->failed == 1) {
      state = "Exit 127";
    } else if (cur_p->is_running == FALSE){
      state = "Done";
    } else if (cur_p->is_running == TRUE) {
      state = "Running";
      amp = '&';
    }
    printf("[%d]%c  %s\t\t%s %c\n", cur_p->index, c, state, cur_p->command, amp);
    cur_p = cur_p->next_bgp;
  }

  /* Remove the head(s) if needed */
  while (bgp_list->is_running == FALSE) {
    bgp *temp = bgp_list;
    bgp_list = bgp_list->next_bgp;
    free(temp);
    if (bgp_list == NULL) {
      break;
    }
  }
  cur_p = bgp_list;
  if (cur_p == NULL) {
    return 0;
  }

  /* List exists. Remove rest of list if needed */
  while (cur_p->next_bgp != NULL) {

    /* Remove non running processes */
    while (cur_p->next_bgp->is_running == FALSE) {
      bgp *next = cur_p->next_bgp;
      cur_p->next_bgp = next->next_bgp;
      free(next);
      if (cur_p->next_bgp == NULL) {
        break;
      }
    }
    if (cur_p->next_bgp == NULL) {
      break;
    }
    cur_p = cur_p->next_bgp;
  }
  return 0;
}

/**
 * This method prints out all the commands that were executed
 * including valid and invalid and the history command that calls this.
 */
int builtin_history(void)
{
  history_node *cur = history_list;
  int index = 0;
  while (cur != NULL) {
    char amp = ' ';
    job_t *job = cur->job;
    if (job->is_background) {
      amp = '&';
    }
    printf("%d ", index);
    int i;
    for (i = 0; i < job->argc; i += 1) {
      printf("%s ", job->argv[i]);
    }
    printf("%c\n", amp);
    index += 1;
    cur = cur->next_node;
  }
  return 0;
}
/**
 * This method waits for all background processes to complete. It outputs the number it is waiting for.
 * @param display is used to identify if the "waiting for" prompt is shown. Used with the exit command.
 * @return -1, no background processes were waited for, 0, at least one background process was waited for
 */
int builtin_wait(int display)
{
  check_bgp();
  int status = 0;
  bgp *cur_p = bgp_list;
  if (cur_p == NULL) {
    return -1;
  }
  /* There exists a list */
  cur_p = bgp_list;
  int index = 0;
  while (cur_p != NULL) {
    if (cur_p->is_running) {
      index += 1;
    }
    cur_p = cur_p->next_bgp;
  }
  if (index == 0) {
    return -1;
  }
  char plural = 's';
  if (index == 1) {
    plural = ' ';
  }
  printf("Waiting for %d job%c.\n", index, plural);
  cur_p = bgp_list;
  index = 0;
  while (cur_p != NULL) {
    if (cur_p->is_running) {
      index += 1;

      waitpid(cur_p->c_pid, &status, 0);
      index += 1;
    }
    cur_p = cur_p->next_bgp;
  }
  check_bgp();
  return 0;
}

/**
 * This method brings a selected or the most recent background (bg) process into the foreground (fg).
 * @param job to looks at arguements
 * @return -1 for no job or invalid syntax, 0 for putting one bg process into the fg
 */
int builtin_fg(job_t *job)
{
    check_bgp();
    int status = 0;
    bgp *cur_p = bgp_list;
    if (cur_p == NULL) {
      printf("No such job.\n");
      return -1;
    } else if (job->argc > 2) {
      printf("Invalid syntax.\n");
      return -1;
    }
    /* There exists a list */
    int index = 1;
    while (cur_p != NULL) {
      /* set index */
      cur_p->index = index;
      index += 1;
      /* set order */
      if (cur_p->next_bgp == NULL) {
        cur_p->order = 2;
      } else if (cur_p->next_bgp->next_bgp == NULL) {
        cur_p->order = 1;
      } else {
        cur_p->order = 0;
      }
      cur_p = cur_p->next_bgp;
    }
    /* Make a mini array of indexes which have running processes */
    bgp *running_arr[index];

    cur_p = bgp_list;
    index = 0;
    while (cur_p != NULL) {
      if (cur_p->is_running) {
        running_arr[index] = cur_p;
        index += 1;
      }
      cur_p = cur_p->next_bgp;
    }
    running_arr[index] = NULL;
    if (index == 0) {
      printf("No such job.\n");
      return -1;
    }

    /* fg */
    if (job->argc == 1) {
      bgp *last_node = running_arr[index - 1];
      printf("%s\n", last_node->command);
      waitpid(last_node->c_pid, &status, 0);
    } else {
      int idx = atoi(job->argv[1]);
      if (idx < 1) {
        printf("Invalid syntax.\n");
        return -1;
      }
      int found = 0;
      int i = 0;
      while (running_arr[i] != NULL) {
        if (running_arr[i]->index == idx) {
          printf("%s\n", running_arr[i]->command);
          waitpid(running_arr[i]->c_pid, &status, 0);
          found = 1;
          break;
        }
        i += 1;
      }
      if (!found) {
        printf("No such job\n");
      }
    }
    return 0;
}
