/*
 * [YOUR NAME HERE]
 *
 * CS441/541: Project 3
 *
 */
#ifndef MYSHELL_H
#define MYSHELL_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* For fork, exec, sleep */
#include <sys/types.h>
#include <unistd.h>
/* For waitpid */
#include <sys/wait.h>

/******************************
 * Defines
 ******************************/
#define TRUE  1
#define FALSE 0

#define MAX_COMMAND_LINE 1024

#define PROMPT ("mysh$ ")


/******************************
 * Structures
 ******************************/
/*
 * A job struct
 */
struct job_t {
    char * full_command;
    int argc;
    char **argv;
    int is_background;
    char * binary;
    struct job_t *next_job;
};
typedef struct job_t job_t;
/**
 * History node for linked list
 */
struct history_node {
    job_t *job;
    struct history_node *next_node;
};
typedef struct history_node history_node;
/**
 * Background process node for linked list
 */
struct bgp {
    int c_pid;
    int is_running;
    int failed;
    int order;
    int index;
    char *command;
    char **argv;
    int argc;
    struct bgp *next_bgp;
};
typedef struct bgp bgp;

/******************************
  Global Variables
 ******************************/

/*
 * Interactive or batch mode
 */
int is_batch = FALSE;
int is_debug = TRUE;

/*
 * Counts
 */
int total_jobs    = 0; /* Jobs that work */
int total_jobs_bg = 0; /* Jobs that work in background */
int total_history = 0; /* Jobs called that work and don't work */

FILE *fp_in; /* Used for batch_mode for reading in files */

bgp* bgp_list = NULL; /* Head of background process linked list */
bgp* bgp_list_end = NULL; /* Tail of background process linked list */
history_node *history_list = NULL; /* Head of history linked list */
history_node *history_list_end = NULL; /* Tail of history linked list */

/******************************
 * Function declarations
 ******************************/

int parse_args_main(int argc, char **argv);

int batch_mode(void);

int interactive_mode(void);


int builtin_exit(void);


int builtin_jobs(void);

int builtin_history(void);

int builtin_wait(int);

int builtin_fg(job_t *);


int execute_command(job_t *);
job_t* break_up_line(char *);

void add(int, job_t *);
void check_bgp();
void insert_space(char *, int);
void copy_job_contents(job_t *, job_t *);
job_t *create_job();
void free_history();

#endif /* MYSHELL_H */
