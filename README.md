# CS441/541 Project 3

## Author(s):

Sahee Thao
Elise Baumgartner


## Date:

10/15/2018


## Description:

This program mimics a bash shell. There are two modes: bash and interactive. Batch mode takes in file names and reads and executes
commands from those files. Interactive mode waits for user's input and then executes the command.


## How to build the software

Use the make file.


## How to use the software

Batch Mode:
  After making the program, type [./mysh] followed by the files the user wants to run.

Interactive Mode:
  After making the program, type [./mysh] with no arguments, this will bring you into interactive mode.
  The user can now type commands it wants the shell to run (in the foreground or background).
  To run a command in the background, the user puts a '&' after the command (i.e. sleep 5 &).
  Multiple commands can be executed on the same line using ';' to separate them (i.e. ls ; date ).
  To exit the shell, the user can either use cntrl-d or the command exit.

## How the software was tested

There are given test files as well as a couple more tests in the tests folder. All test for different checks.
More information about the tests in the test folder included in README in test folder


## Known bugs and problem areas

File redirection not implemented.
Batch mode sometimes reads non real commands in background as real commands in background.
Example: 
-----------------------------------
sleep 1 & sleep 2 & sleep 3 &
ls
mkdir coolhat
date
pewpew & snowball & steamroll &
canoe &
mushroom &
jobs
exit
-----------------------------------
