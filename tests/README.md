test1

  This file will test the functionality of the written functions fg, wait, jobs, history, exit.

test2

    This file will test the case where the "exit" command in middle of file.
    Upon reaching the exit command, the program will wait for the background processes still running and then exit that file.

test3

  This file will tests the case where the command has multiple white spaces and the case where the usage of & or ; doesn't contain wait spaces on either side.

test4

  This file will test a command line with over 1024 arguments;

test5
  The file will test the program's ability to handle invalid commands
